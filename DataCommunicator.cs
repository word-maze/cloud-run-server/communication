﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;
using DataCore.Responses;

namespace Communication
{
    public class DataCommunicator : IData
    {
        private ISerialiser _serialiser;
        private readonly String _baseUrl;

        public DataCommunicator(String baseUrl, ISerialiser serialiser)
        {
            this._baseUrl = baseUrl;
            _serialiser = serialiser;
        }

        public async Task<PuzzleMap> GetPuzzleMap(String date, Int32 shape, String uid)
        {
            String puzzleMapData = await Communicator.Client.GetStringAsync($"{_baseUrl}/api/data/{date}/{shape}/map/{uid}");

            if (String.IsNullOrEmpty(puzzleMapData))
            {
                return null;
            }

            return JsonSerializer.Deserialize<PuzzleMap>(puzzleMapData);
        }

        public async Task<String> GetShapeData(Int32 shape)
        {
            return await Communicator.Client.GetStringAsync($"{_baseUrl}/api/data/shape/{shape}");
        }

        public async Task<FoundPuzzleWords> GetFoundWords(String date, Int32 shape, String token)
        {
            String foundData = await Communicator.Client.GetStringAsync($"{_baseUrl}/api/data/{date}/{shape}/{token}/words");

            return _serialiser.Deserialise<FoundPuzzleWords>(foundData);
        }
    }
}
