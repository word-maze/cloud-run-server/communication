﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using DataCore.Interfaces;
using DataCore.Responses;

namespace Communication
{
    public class OperationsCommunicator : IOperations
    {
        private ISerialiser _serialiser;
        private readonly String _baseUrl;

        public OperationsCommunicator(String baseUrl, ISerialiser serialiser)
        {
            this._baseUrl = baseUrl;
            _serialiser = serialiser;
        }

        public async Task GenerateDailyPuzzles()
        {
            // This should not be exposed to a client. This is cron-job only.
        }

        public async Task<FoundWord> CheckWord(String date, Int32 shape, String uid, WordData word)
        {
            String wordData = _serialiser.Serialise(word);
            var response = await Communicator.Client.PostAsync($"{_baseUrl}/api/operations/{date}/{shape}/{uid}/check", new StringContent(wordData));
            String data = await response.Content.ReadAsStringAsync();
            return _serialiser.Deserialise<FoundWord>(data);
        }

        public async Task<FoundPuzzleWords> BatchCheck(String date, Int32 shape, String token, WordList words)
        {
            String wordData = _serialiser.Serialise(words);
            var response = await Communicator.Client.PostAsync($"{_baseUrl}/api/operations/{date}/{shape}/{token}/batchcheck", new StringContent(wordData));
            String data = await response.Content.ReadAsStringAsync();
            return _serialiser.Deserialise<FoundPuzzleWords>(data);
        }

        public async Task<WordList> RequestHint(String date, Int32 shape, String token, Boolean buy)
        {
            var response = await Communicator.Client.PostAsync($"{_baseUrl}/api/operations/{date}/{shape}/{token}/hint?buy={buy}", new StringContent(""));
            String data = await response.Content.ReadAsStringAsync();
            return _serialiser.Deserialise<WordList>(data);
        }
    }
}
