﻿using System;
using System.Net;
using System.Net.Http;
using DataCore.Interfaces;

namespace Communication
{
    public class Communicator : IServer
    {
        private static readonly HttpClientHandler ClientHandler = new HttpClientHandler()
        {
            AutomaticDecompression = DecompressionMethods.GZip
        };
        public static readonly HttpClient Client = new HttpClient(ClientHandler);
        private const String StartingURL = "https://test-server-wnetgknjda-ew.a.run.app";

        public Communicator(Version clientVersion, ISerialiser serialiser = null)
        {
            Communicator.Client.DefaultRequestHeaders.UserAgent.ParseAdd("Remote Client Library");
            IVersion versionAPI = new VersionCommunicator(StartingURL, serialiser);
            var response = versionAPI.GetAssociatedVersion(clientVersion).Result;
            String baseUrl = StartingURL;

            UserAPI = new UserCommunicator(baseUrl, serialiser);
            DataAPI = new DataCommunicator(baseUrl, serialiser);
            OperationsAPI = new OperationsCommunicator(baseUrl, serialiser);
            VersionAPI = new VersionCommunicator(baseUrl, serialiser);
        }

        public Communicator(String baseUrl, ISerialiser serialiser = null)
        {
            UserAPI = new UserCommunicator(baseUrl, serialiser);
            DataAPI = new DataCommunicator(baseUrl, serialiser);
            OperationsAPI = new OperationsCommunicator(baseUrl, serialiser);
            VersionAPI = new VersionCommunicator(baseUrl, serialiser);
        }

        public IOperations OperationsAPI { get; }
        public IData DataAPI { get; }
        public IUser UserAPI { get; }
        public IVersion VersionAPI { get; }
    }
}
