﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;

namespace Communication
{
    public class UserCommunicator : IUser
    {
        private ISerialiser _serialiser;
        private readonly String _baseUrl;

        public UserCommunicator(String baseUrl, ISerialiser serialiser)
        {
            this._baseUrl = baseUrl;
            _serialiser = serialiser;
        }

        public async Task<String> CreateNewUser()
        {
            return await Communicator.Client.GetStringAsync($"{_baseUrl}/api/user/new");
        }

        public async Task Combine(String oldToken, String newToken)
        {
            await Communicator.Client.PostAsync($"{_baseUrl}/api/user/{oldToken}/combine/{newToken}", new StringContent(""));
        }

        public async Task Announce(String uid)
        {
            await Communicator.Client.PostAsync($"{_baseUrl}/api/user/{uid}/announce", new StringContent(""));
        }

        public async Task<UserData> GetUserData(String uid)
        {
            String userData = await Communicator.Client.GetStringAsync($"{_baseUrl}/api/user/{uid}");
            return _serialiser.Deserialise<UserData>(userData);
        }
    }
}
