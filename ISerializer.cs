﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Communication
{
    public interface ISerialiser
    {
        T Deserialise<T>(String data) where T : class, new();
        String Serialise(Object data);
    }
}
