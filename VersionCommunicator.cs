﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;

namespace Communication
{
    public class VersionCommunicator : IVersion
    {
        private ISerialiser _serialiser;
        private readonly String _baseUrl;

        public VersionCommunicator(String baseUrl, ISerialiser serialiser)
        {
            this._baseUrl = baseUrl;
            _serialiser = serialiser;
        }

        public async Task<VersionData> GetAssociatedVersion(Version clientVersion)
        {
            String versionData = await Communicator.Client.GetStringAsync($"{_baseUrl}/api/version/{clientVersion}");
            return _serialiser.Deserialise<VersionData>(versionData);
        }

        public async Task Declare(Version serverVersion = null, String url = null)
        {
            await Communicator.Client.PostAsync($"{_baseUrl}/api/version/new", new StringContent(""));
        }

        public async Task AssociateVersions(Version clientVersion, Version serverVersion = null)
        {
            await Communicator.Client.PostAsync($"{_baseUrl}/api/version/{clientVersion}/associate", new StringContent(""));
        }
    }
}
